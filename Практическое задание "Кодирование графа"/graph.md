```mermaid
flowchart LR

%% задаем структуру, виды связей и параметры
p0[/I/] -.- h1;
h0[/Тслом = 50ч/];
h2 -.->par3((сост));
h7 -.->part3((сост));
par1((режим)) -.->h3;
par2((мастер))-.->h4;
h5 -.->par2((мастер));
h7 -.->par2((мастер));
h5 -.->par4((Трем));
par4((Трем)) -.-> h6;

p1[/i/] -.- h8;
h15[/Треб = 9ч/];
h8 -.->par1;
h10 -.->par1;
par2 -.->h13;
h14 -.->par4;

h1([ВРЕМЯ=Тслом])==> h2[сост:=<br>сломан];
h2 ==> h3([режим=<br>работа]);
h3 ==> h4([мастер<br>=своб]);
h4 ==> h5[мастер:=занят<br>Трем: =func/*/.];
h5 ==> h6([ВРЕМЯ=Трем]);
h6 ==> h7[сост:=рабочий<br>мастер:=своб<br>Тслом:=func/*/.];
h7 ==> h1;

h8[режим:=<br>отдых]==> h9([ВРЕМЯ=Траб]);
h9 ==> h10[режим:=работа<br>Тодд:=func___];
h10 ==> h11([ВРЕМЯ=Тотд]);
h11 ==> h12[Траб:=func];
h12 ==> h13{мастер<br>=...};
h13 ==> |"…= занят"| h14[Трем:=Трем+<br>Траб-ВРЕМЯ];
h13 ==> |"…= своб" | h8;
h14 ==> h8;

%% задаем раскраску
classDef cond fill: #bee,stroke: #aaa,stroke-width:1px;
classDef state fill: #9e8,stroke: #333,stroke-width:1px;
classDef navig fill:#ffff00,stroke: #333,stroke-width:1px;
class h5,h8,h2 state;
class h1,h3,h4,h7 cond;
class h6 navig;
style par1 fill:#fcc,stroke: #111,stroke-width:2px;
style par4 fill:#ccc,stroke: #555,stroke-width:2px:
